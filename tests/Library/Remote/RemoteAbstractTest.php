<?php
/**
 * Created by PhpStorm.
 * User: tgray
 * Date: 06/01/2016
 * Time: 21:32
 */

namespace Library\Remote;

use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;


class RemoteAbstractTest extends \PHPUnit_Framework_TestCase
{

    public function testClientConstructsWithValidUrl()
    {
        $url = 'http://google.co.uk';

        $mock = $this->getMockForAbstractClass(RemoteAbstract::class, [$url]);

        $this->assertInstanceOf(RemoteAbstract::class, $mock);
    }

    public function testClientDoesNotConstructWithArrayUrl()
    {
        $url = ['lol'];

        $this->setExpectedException(InvalidArgumentException::class, 'URI must be a string or UriInterface');

        $mock = $this->getMockForAbstractClass(RemoteAbstract::class, [$url]);
    }

    public function testClientDoesNotConstructWithEmptyUrl()
    {
        $url = '';

        $this->setExpectedException(InvalidArgumentException::class, 'You must pass a non-empty url');

        $mock = $this->getMockForAbstractClass(RemoteAbstract::class, [$url]);
    }

    public function testWillNotAcceptHeadersNonArray()
    {
        $url = 'http://google.co.uk';

        $mock = $this->getMock(RemoteAbstract::class, [], [$url]);

        $reflectionClass = new \ReflectionProperty(RemoteAbstract::class, 'headers');

        $reflectionClass->setAccessible(true);

        $reflectionClass->setValue($mock, new \stdClass());

        $reflectionMethod = new \ReflectionMethod(RemoteAbstract::class, 'generateClientConfig');

        $reflectionMethod->setAccessible(true);

        $this->setExpectedException('RuntimeException', 'You must provide me with an array of headers');

        $reflectionMethod->invoke($mock, $url);
    }

    public function testDetectsContentTypeOk()
    {
        $url = 'http://google.co.uk';
        $responseMock = $this->getMock(ResponseInterface::class);

        $responseMock->expects($this->once())->method('getHeader')->will($this->returnValue([
            'application/lol; foo=bar',
            'application/json; true=yes'
        ]));

        $remoteMock = $this->getMockForAbstractClass(RemoteAbstract::class, [$url]);

        $response = $remoteMock->isResponseJsonContentType($responseMock);

        $this->assertTrue($response);
    }

    public function testDecodesJsonStream()
    {
        $contents = json_encode(['test' => 'body']);

        $url = 'http://google.co.uk';
        $responseMock = $this->getMock(ResponseInterface::class);

        $responseMock->expects($this->once())->method('getHeader')->will($this->returnValue([
            'application/lol; foo=bar',
            'application/json; true=yes'
        ]));

        $bodyMock = $this->getMock(StreamInterface::class);

        $bodyMock->expects($this->once())->method('getContents')->will($this->returnValue($contents));

        $responseMock->expects($this->once())->method('getBody')->will($this->returnValue($bodyMock));

        $remoteMock = $this->getMockForAbstractClass(RemoteAbstract::class, [$url]);

        $response = $remoteMock->decodeJsonStream($responseMock);

        $this->assertEquals(json_decode($contents, true), $response);
    }

    public function testFailsWhenContentTypeIsNotJson()
    {
        $contents = json_encode(['test' => 'body']);

        $url = 'http://google.co.uk';
        $responseMock = $this->getMock(ResponseInterface::class);

        $responseMock->expects($this->once())->method('getHeader')->will($this->returnValue([
            'application/lol; foo=bar',
            'application/NOTJSON; true=yes'
        ]));


        $remoteMock = $this->getMockForAbstractClass(RemoteAbstract::class, [$url]);

        $this->setExpectedException('RuntimeException', 'was not content type json');

        $response = $remoteMock->decodeJsonStream($responseMock);

    }

    public function testFailsWhenBodyIsNotJson()
    {
        $contents = '!!';

        $url = 'http://google.co.uk';
        $responseMock = $this->getMock(ResponseInterface::class);

        $responseMock->expects($this->once())->method('getHeader')->will($this->returnValue([
            'application/lol; foo=bar',
            'application/json; true=yes'
        ]));

        $bodyMock = $this->getMock(StreamInterface::class);

        $bodyMock->expects($this->once())->method('getContents')->will($this->returnValue($contents));

        $responseMock->expects($this->once())->method('getBody')->will($this->returnValue($bodyMock));

        $remoteMock = $this->getMockForAbstractClass(RemoteAbstract::class, [$url]);

        $this->setExpectedException('RuntimeException', 'decode the json response body');

        $response = $remoteMock->decodeJsonStream($responseMock);

    }


}