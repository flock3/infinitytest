<?php
/**
 * Created by PhpStorm.
 * User: tgray
 * Date: 06/01/2016
 * Time: 21:35
 */

namespace Library\Remote\FoodStandards;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\BadResponseException;

class FoodStandardsEstablishmentsClientTest extends \PHPUnit_Framework_TestCase
{
    public function testFetchesOkWithEstablishments()
    {
        $data = [
            'establishments' => ['name' => 'foo', 'bar' => 'value'],
        ];

        $mock = new MockHandler([
            new Response(200, ['content-type' => 'application/json'], json_encode($data)),
        ]);

        $handler = HandlerStack::create($mock);
        $mockClient = new Client(['handler' => $handler]);

        $establishmentsClient = new FoodStandardsEstablishmentsClient('http://google.co.uk');

        $this->assertInstanceOf(FoodStandardsEstablishmentsClient::class, $establishmentsClient);

        $clientProperty = new \ReflectionProperty(FoodStandardsAuthoritiesClient::class, 'client');

        $clientProperty->setAccessible(true);

        $clientProperty->setValue($establishmentsClient, $mockClient);

        $response = $establishmentsClient->fetchEstablishments(1);

        $this->assertInternalType('array', $response);
        $this->assertArrayHasKey('name', $response);
    }

    public function testFailsWithInvalidEstablishments()
    {
        $data = [
            'not establishments' => ['name' => 'foo', 'bar' => 'value'],
        ];

        $mock = new MockHandler([
            new Response(200, ['content-type' => 'application/json'], json_encode($data)),
        ]);

        $handler = HandlerStack::create($mock);
        $mockClient = new Client(['handler' => $handler]);

        $establishmentsClient = new FoodStandardsEstablishmentsClient('http://google.co.uk');

        $this->assertInstanceOf(FoodStandardsEstablishmentsClient::class, $establishmentsClient);

        $clientProperty = new \ReflectionProperty(FoodStandardsAuthoritiesClient::class, 'client');

        $clientProperty->setAccessible(true);

        $clientProperty->setValue($establishmentsClient, $mockClient);

        $this->setExpectedException('RuntimeException', 'by the FSA api were not in a format we expected');

        $response = $establishmentsClient->fetchEstablishments(1);
    }

    public function testFailsBadAuthorityId()
    {
        $establishmentsClient = new FoodStandardsEstablishmentsClient('http://google.co.uk');

        $this->setExpectedException('InvalidArgumentException', 'integer local authority id');

        $establishmentsClient->fetchEstablishments(new \stdClass());


    }

    public function testFailesWithFivexx()
    {
        $mock = new MockHandler([
            new BadResponseException(
                'Something went wrong or something',
                new Request('GET', '/authorities'),
                new Response(201)),
        ]);

        $handler = HandlerStack::create($mock);
        $mockClient = new Client(['handler' => $handler]);

        $establishmentsClient = new FoodStandardsEstablishmentsClient('http://google.co.uk');

        $this->assertInstanceOf(FoodStandardsEstablishmentsClient::class, $establishmentsClient);

        $clientProperty = new \ReflectionProperty(FoodStandardsAuthoritiesClient::class, 'client');

        $clientProperty->setAccessible(true);

        $clientProperty->setValue($establishmentsClient, $mockClient);

        $this->setExpectedException('RuntimeException', 'error retrieving the list of establishment');

        $response = $establishmentsClient->fetchEstablishments(1);
    }
}