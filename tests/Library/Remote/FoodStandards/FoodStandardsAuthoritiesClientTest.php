<?php
/**
 * Created by PhpStorm.
 * User: tgray
 * Date: 06/01/2016
 * Time: 21:35
 */

namespace Library\Remote\FoodStandards;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\BadResponseException;

class FoodStandardsAuthoritiesClientTest extends \PHPUnit_Framework_TestCase
{
    public function testFetchMockValidAuthorities()
    {
        $data = [
            'authorities' => ['name' => 'foo', 'bar' => 'value'],
        ];

        $mock = new MockHandler([
            new Response(200, ['content-type' => 'application/json'], json_encode($data)),
        ]);

        $handler = HandlerStack::create($mock);
        $mockClient = new Client(['handler' => $handler]);

        $authorityClient = new FoodStandardsAuthoritiesClient('http://google.co.uk');

        $this->assertInstanceOf(FoodStandardsAuthoritiesClient::class, $authorityClient);

        $clientProperty = new \ReflectionProperty(FoodStandardsAuthoritiesClient::class, 'client');

        $clientProperty->setAccessible(true);

        $clientProperty->setValue($authorityClient, $mockClient);

        $authorities = $authorityClient->fetchAuthorities();

        $this->assertInternalType('array', $authorities);
        $this->assertEquals($data['authorities'], $authorities);
    }

    public function testFetchMockFailsBadHeader()
    {
        $data = [];

        $mock = new MockHandler([
            new Response(200, ['content-type' => 'Not json'], json_encode($data)),
        ]);

        $handler = HandlerStack::create($mock);
        $mockClient = new Client(['handler' => $handler]);

        $authorityClient = new FoodStandardsAuthoritiesClient('http://google.co.uk');

        $this->assertInstanceOf(FoodStandardsAuthoritiesClient::class, $authorityClient);

        $clientProperty = new \ReflectionProperty(FoodStandardsAuthoritiesClient::class, 'client');

        $clientProperty->setAccessible(true);

        $clientProperty->setValue($authorityClient, $mockClient);

        $this->setExpectedException('RuntimeException', 'json');

        $authorities = $authorityClient->fetchAuthorities();
    }


    public function testFetchMockFailsFivexx()
    {
        $data = [];

        $mock = new MockHandler([
            new BadResponseException(
                'Something went wrong or something',
                new Request('GET', '/authorities'),
                new Response(201)),
        ]);

        $handler = HandlerStack::create($mock);
        $mockClient = new Client(['handler' => $handler]);

        $authorityClient = new FoodStandardsAuthoritiesClient('http://google.co.uk');

        $this->assertInstanceOf(FoodStandardsAuthoritiesClient::class, $authorityClient);

        $clientProperty = new \ReflectionProperty(FoodStandardsAuthoritiesClient::class, 'client');

        $clientProperty->setAccessible(true);

        $clientProperty->setValue($authorityClient, $mockClient);

        $this->setExpectedException('RuntimeException', 'There was an error retrieving the list of authorities');

        $authorities = $authorityClient->fetchAuthorities();
    }

    public function testFetchFailsBadAuthorities()
    {
        $data = [
            'not authorities' => ['name' => 'foo', 'bar' => 'value'],
        ];

        $mock = new MockHandler([
            new Response(200, ['content-type' => 'application/json'], json_encode($data)),
        ]);

        $handler = HandlerStack::create($mock);
        $mockClient = new Client(['handler' => $handler]);

        $authorityClient = new FoodStandardsAuthoritiesClient('http://google.co.uk');

        $this->assertInstanceOf(FoodStandardsAuthoritiesClient::class, $authorityClient);

        $clientProperty = new \ReflectionProperty(FoodStandardsAuthoritiesClient::class, 'client');

        $clientProperty->setAccessible(true);

        $clientProperty->setValue($authorityClient, $mockClient);

        $this->setExpectedException('RuntimeException', 'The authorities returned by the FSA api');

        $authorityClient->fetchAuthorities();
    }

}