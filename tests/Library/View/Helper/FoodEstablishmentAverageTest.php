<?php
/**
 * Created by PhpStorm.
 * User: tgray
 * Date: 06/01/2016
 * Time: 21:07
 */

namespace Library\View\Helper;


class FoodEstablishmentAverageTest extends \PHPUnit_Framework_TestCase
{
    public function testReturnsAcceptableAverage()
    {
        $data = [
            ['RatingValue' => 1],
            ['RatingValue' => 2],
            ['RatingValue' => 3],
            ['RatingValue' => 3],
            ['RatingValue' => 5],
        ];

        $returned = FoodEstablishmentAverage::generateAverageScore($data);

        $this->assertInternalType('array', $returned);
        $this->assertCount(4, $returned);

        $this->assertEquals(1, $returned[1]['count'], 'RatingValue 1 is wrong');
        $this->assertEquals(20, $returned[1]['average'], 'RatingValue 1 average is wrong');

        $this->assertEquals(1, $returned[2]['count'], 'RatingValue 2 is wrong');
        $this->assertEquals(20, $returned[2]['average'], 'RatingValue 2 average is wrong');

        $this->assertEquals(2, $returned[3]['count'], 'RatingValue 3 is wrong');
        $this->assertEquals(40, $returned[3]['average'], 'RatingValue 3 average is wrong');

        $this->assertEquals(1, $returned[5]['count'], 'RatingValue 5 is wrong');
        $this->assertEquals(20, $returned[5]['average'], 'RatingValue 5 average is wrong');
    }

    public function testFailsWhenNonArrayGiven()
    {
        $data = 'LOL';

        $this->setExpectedException(\PHPUnit_Framework_Error::class, 'must be of the type array');
        FoodEstablishmentAverage::generateAverageScore($data);
    }

    public function testThrowsErrorOnMissingValid()
    {
        $data = [
            ['LolValue' => 1],
            ['RatingValue' => 2],
            ['RatingValue' => 3],
            ['RatingValue' => 3],
            ['RatingValue' => 5],
        ];

        $this->setExpectedException('InvalidArgumentException', 'establishment returned');

        FoodEstablishmentAverage::generateAverageScore($data);

    }
}