<?php
use Library\Remote\FoodStandards\FoodStandardsAuthoritiesClient;
use Library\Remote\FoodStandards\FoodStandardsEstablishmentsClient;

return function(\Slim\App $app)
{
    $app->get('/', function(\Slim\Http\Request $request, \Slim\Http\Response $response)
    {
        $foodStandardsClient = new FoodStandardsAuthoritiesClient(
            $this[\Library\Config\ConfigEnum::FOOD_STANDARDS_API_URL]
        );

        $authorities = $foodStandardsClient->fetchAuthorities();

        return $this->view->render($response, 'index.twig', [
            'authorities' => $authorities,
        ]);
    });

    $app->get('/authority', function(\Slim\Http\Request $request, \Slim\Http\Response $response)
    {
        $authorityId = $request->getParam('authority');

        if(!is_numeric($authorityId))
        {
            throw new RuntimeException('There seems to be a problem with the authority you selected.');
        }

        $foodStandardsClient = new FoodStandardsEstablishmentsClient(
            $this[\Library\Config\ConfigEnum::FOOD_STANDARDS_API_URL]
        );

        $establishments = $foodStandardsClient->fetchEstablishments($authorityId);

        if(empty($establishments))
        {
            throw new RuntimeException('We could not find any establishments for that authority.');
        }


        $averageScores = \Library\View\Helper\FoodEstablishmentAverage::generateAverageScore($establishments);

        return $this->view->render($response, 'authority.twig', [
            'establishments' => $establishments,
            'scores' => $averageScores,
        ]);
    });
};
