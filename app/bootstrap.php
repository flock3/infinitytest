<?php

use Library\Config\ConfigEnum;

return function (Slim\App $app, $appConfig)
{
    $container = $app->getContainer();

    $container['view'] = function ($container) use($appConfig)
    {
        $debug = $appConfig[ConfigEnum::APP_SETTINGS][ConfigEnum::DEBUG];

        $view = new \Slim\Views\Twig(
            $appConfig[ConfigEnum::TEMPLATE_PATH],
            [
                'cache' => $appConfig[ConfigEnum::CACHE_DIR],
                'debug' => $debug
            ]
        );

        if($debug)
        {
            // If we're in debug mode, add the debug extension
            $view->addExtension(new Twig_Extension_Debug());
        }

        $view->addExtension(new \Slim\Views\TwigExtension(
            $container['router'],
            $container['request']->getUri()
        ));

        return $view;
    };

    $container['errorHandler'] = function ($container)
    {
        return function ($request, $response, $exception) use ($container)
        {
            return $container->view->render($response, 'error.twig', [
                'error' => $exception,
                'debug' => $container[ConfigEnum::APP_SETTINGS][ConfigEnum::DEBUG]
            ]);
        };
    };
};