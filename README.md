# Tech Test

## Installation Instructions

This projects has a number of dependencies. These instructions have been designed for a *nix machine, and will probably not work on Windows.

### External Dependencies:
* composer (http://getcomposer.org)
* php 5.4.x
* Web server


### Setup Steps
* Step 1: Clone out the project
* Step 2: In project root, do: `composer.phar install` (this will install all dependencies)
* Step 3: Copy `build.properties.dist` to `build.properties` in project root
* Step 4: change any debug values that you wish (defaults are acceptable)
* Step 5: execute: `vendor/bin/phing` (this will copy in all config values into a live config)
* Step 6: Check the `cache` folder in the project root is writable by the web server
* Step 7: Setup your web server to point to project root, and treat index.php as default file.
* Step 8: Load up the app and poke around.

### Testing

A phpunit test suite (and accompanying xml file) is included, if you have xdebug turned on in your PHP application, it will generate a Code Coverage report, otherwise it will fail back to not generating one.

To execute tests, simply execute: `vendor/bin/phpunit`

## General Readme

### Code dependencies

I've used Slim Framework and Guzzle on this project, I'm relatively familiar with them both (as I use them on my current projects) and they're great little tools.

I am also using twig templating engine to bring up my shoddy frontend.

## Project thoughts

I was rather frustrated to find that the `/ratings` interface on the FHRS API doesn't actually return all of the ratings that can appear in the establishments themselves. I'm assuming that this is because a "not yet visited" is not actually a rating.

It being late in the day, I have simply left it at it returning the names as given by the establishments interface, however I had considered (and actually implemented) the ratings call, before finding that out.

The test is relatively unclear about what to do about non-ratings such as these, so I decided to ignore it (because it's late, and I am running out of time) and put it in here.

Other than that, pretty simple application, test coverage is acceptable, and I think a reasonable demonstration of my ability to write weird intermediary applications in a few hours.

**Total time spent: about 4 hours, including 1 hour messing around setting up the app.**




