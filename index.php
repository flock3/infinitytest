<?php

require(__DIR__ . '/vendor/autoload.php');

// Rely on PHP default include path.
$configFile = __DIR__ . '/app/config.php';

if(!file_exists($configFile))
{
    throw new RuntimeException('You must provide a configuration file. Please check the README.');
}

// Load in our app config container
$appConfig = require($configFile);

/**
 * Load up the slim app, inject the config container.
 */
$app = new Slim\App($appConfig);

// Load up our application bootstrap self-contained anon.
$bootstrap = require(__DIR__ . '/app/bootstrap.php');

// Bootstrap our application
$bootstrap($app, $appConfig);

// Require our routes inside their anon.
$routeCallback = require(__DIR__ . '/app/routes/index.php');

// Call our route callback.
$routeCallback($app);


$app->run();
