<?php
/**
 * Created by PhpStorm.
 * User: tgray
 * Date: 04/01/2016
 * Time: 21:03
 */

namespace Library\Remote\FoodStandards;

use RuntimeException;

class FoodStandardsEstablishmentsClient extends FoodStandardsAbstract
{
    /**
     * Fetch the list of establishments
     *
     * @param integer $localAuthorityId
     *
     * @return array
     */
    public function fetchEstablishments($localAuthorityId)
    {
        if(!is_numeric($localAuthorityId))
        {
            throw new \InvalidArgumentException('You must provide me with an integer local authority id!');
        }

        try
        {
            $response = $this->client->get(
                'establishments',
                [
                    'query' => [ // Get params to be added onto query string
                        'localAuthorityId' => $localAuthorityId
                    ]
                ]
            );
        } catch(\Exception $error)
        {
            throw new RuntimeException('There was an error retrieving the list of establishments.', 0, $error);
        }

        $establishments = $this->decodeJsonStream($response);

        if(!$this->establishmentsFormatIsValid($establishments))
        {
            throw new RuntimeException('The authorities returned by the FSA api were not in a format we expected.');
        }

        return $establishments['establishments'];
    }


    /**
     * Checks to see that the authorities response was valid.
     *
     * @param $authorities
     * @return bool
     */
    protected function establishmentsFormatIsValid($authorities)
    {
        if(!is_array($authorities) || !array_key_exists('establishments', $authorities))
        {
            return false;
        }

        return true;
    }
}