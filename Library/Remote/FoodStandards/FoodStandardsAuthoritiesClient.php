<?php
/**
 * Created by PhpStorm.
 * User: tgray
 * Date: 04/01/2016
 * Time: 21:03
 */

namespace Library\Remote\FoodStandards;

use RuntimeException;

class FoodStandardsAuthoritiesClient extends FoodStandardsAbstract
{
    /**
     * Fetch the list of authorities
     *
     * @return array
     */
    public function fetchAuthorities()
    {
        try
        {
            $response = $this->client->get('/authorities/basic');
        } catch(\Exception $error)
        {
            throw new RuntimeException('There was an error retrieving the list of authorities.', 0, $error);
        }

        $authorities = $this->decodeJsonStream($response);

        if(!$this->authoritiesFormatIsValid($authorities))
        {
            throw new RuntimeException('The authorities returned by the FSA api were not in a format we expected.');
        }

        return $authorities['authorities'];
    }

    /**
     * Checks to see that the authorities response was valid.
     *
     * @param $authorities
     * @return bool
     */
    protected function authoritiesFormatIsValid($authorities)
    {
        if(!is_array($authorities) || !array_key_exists('authorities', $authorities))
        {
            return false;
        }

        return true;
    }
}