<?php
/**
 * Created by PhpStorm.
 * User: tgray
 * Date: 04/01/2016
 * Time: 21:04
 */

namespace Library\Remote\FoodStandards;


use Library\Remote\RemoteAbstract;

abstract class FoodStandardsAbstract extends RemoteAbstract
{
    protected $headers = [
        'x-api-version' => 2,
        'Accept'        => 'application/json',
    ];
}