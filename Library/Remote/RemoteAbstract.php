<?php
/**
 * Created by PhpStorm.
 * User: tgray
 * Date: 04/01/2016
 * Time: 21:01
 */

namespace Library\Remote;


use GuzzleHttp\Client;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;

abstract class RemoteAbstract
{
    /**
     * @var Client
     */
    protected $client;

    protected $headers = [];

    /**
     * RemoteAbstract constructor.
     *
     * @param string $apiBaseUrl Base URI of the API we're accessing (i.e. google.co.uk/api/)
     */
    public function __construct($apiBaseUrl)
    {
        if(empty($apiBaseUrl))
        {
            throw new InvalidArgumentException('You must pass a non-empty url!');
        }

        $clientConfig = $this->generateClientConfig($apiBaseUrl);

        $this->client = new Client($clientConfig);
    }

    /**
     * Generates a Guzzle Client configuration and returns it.
     * @param string $baseUri
     *
     * @return array
     */
    protected function generateClientConfig($baseUri)
    {
        $headers = $this->getDefaultHeaders();

        if(!is_array($headers))
        {
            throw new \RuntimeException('You must provide me with an array of headers to use as default');
        }

        return [
            'headers' => $headers,
            'base_uri' => $baseUri
        ];
    }

    /**
     * decodeJsonStream accepts a ResponseInterface and attempts to decode the returned output.
     *
     * @param ResponseInterface $response
     *
     * @return mixed
     */
    public function decodeJsonStream(ResponseInterface $response)
    {
        if(!$this->isResponseJsonContentType($response))
        {
            throw new \RuntimeException('The response from the remote API was not content type json!');
        }

        $body = $response->getBody()->getContents();

        $decoded = json_decode($body, true);

        if(JSON_ERROR_NONE === json_last_error())
        {
            return $decoded;
        }

        throw new \RuntimeException('Could not decode the json response body. An error occurred.');
    }

    /**
     * Method checks the content type header from the response to confirm that one of the header properties
     * contains the magic words: "application/json".
     *
     * @param ResponseInterface $response
     *
     * @return bool
     */
    public function isResponseJsonContentType(ResponseInterface $response)
    {
        $contentTypes = $response->getHeader('content-type');

        $isJson = false;
        foreach($contentTypes as $contentType)
        {
            if(false !== stripos($contentType, 'application/json'))
            {
                $isJson = true;
            }
        }

        return $isJson;
    }

    /**
     * Method is called on construction of the client (on class construction) contains the default headers that
     * will be added to every request.
     *
     * @return array
     */
    protected function getDefaultHeaders()
    {
        return $this->headers;
    }
}