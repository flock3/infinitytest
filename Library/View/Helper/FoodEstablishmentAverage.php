<?php
/**
 * Created by PhpStorm.
 * User: tgray
 * Date: 06/01/2016
 * Time: 20:09
 */

namespace Library\View\Helper;


use InvalidArgumentException;

class FoodEstablishmentAverage
{
    public static function generateAverageScore(array $establishments)
    {
        $keyName   = 'RatingValue';
        $ratings = [];
        foreach($establishments as &$establishment)
        {
            if(!array_key_exists($keyName, $establishment))
            {
                throw new InvalidArgumentException('An establishment returned with an incorrect format.');
            }

            $ratingKey = $establishment[$keyName];

            if(!array_key_exists($ratingKey, $ratings))
            {
                $ratings[$ratingKey] = 0;
            }

            $ratings[$establishment[$keyName]]++;
        }

        $totalEstablishments = array_sum($ratings);

        foreach($ratings as $key => $establishmentCount)
        {
            $ratings[$key] = [
                'count' => $establishmentCount,
                'average' => ($establishmentCount / $totalEstablishments) * 100
            ];
        }

        return $ratings;
    }
}