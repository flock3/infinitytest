<?php
/**
 * Created by PhpStorm.
 * User: tgray
 * Date: 04/01/2016
 * Time: 20:07
 */

namespace Library\Config;


class ConfigEnum
{
    /* Cache settings */

    // Directory used to cache the JSON responses from the FSA API
    const CACHE_DIR = 'cacheDir';

    /* App settings */
    const APP_SETTINGS  = 'settings';
    const DEBUG         = 'displayErrorDetails';
    const TEMPLATE_PATH = 'templatePath';

    /* Remote API settings */
    const FOOD_STANDARDS_API_URL = 'foodStandardsApiUrl';

}